<?php

session_start();

?>


<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Practicing the Web</title>
    <link rel="stylesheet" href="Styles/bootstrap.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="Styles/jquery-ui.css">
    <link rel="stylesheet" href="Styles/jquery-ui.structure.css">
    <link rel="stylesheet" href="Styles/jquery-ui.theme.css">
    <link rel="stylesheet" href="Styles/circletimer.css">
    <link rel="stylesheet" href="Styles/mystyle.css">
</head>

<body>
 
 <div id="bar1"></div>
 <div id="bar2"></div>
 <div id="avatar1"></div>
 <div id="avatar2"></div>
 <div id="timer1"></div>
 <div id="timer2"></div>
 <input type="text" class="form-control" id="input1" value="<?php echo $_SESSION["First"] ?>" disabled>
 <input type="text" class="form-control" id="input2" value="<?php echo $_SESSION["Second"] ?>" disabled>
 
 <br>
 <br>
 
<div class="container">
 <div class="jumbotron">
  
     <div id="A-block"></div>
     <div id="B-block"></div>
     <div id="C-block"></div>
     <div id="D-block"></div>
     <div id="E-block"></div>
     <div id="F-block"></div>
     <div id="G-block"></div>
     <div id="H-block"></div>
     <div id="I-block"></div>
     
     
      <div class="row">
         
        <div class="col-lg-3 A-block">        
           <div class="square untouch" id="A1"></div>
                     
           <div class="square untouch" id="A2"></div>
                     
           <div class="square untouch" id="A3"></div>
        </div>
           
        <div class="col-lg-3 B-block">     
           <div class="square untouch" id="B1"></div>
                     
           <div class="square untouch" id="B2"></div>
                     
           <div class="square untouch" id="B3"></div>
        </div>
           
        <div class="col-lg-3 C-block">     
           <div class="square untouch" id="C1"></div>
                     
           <div class="square untouch" id="C2"></div>
                     
           <div class="square untouch" id="C3"></div>
        </div>
           
      </div>
  
  
      <div class="row">
           
        <div class="col-lg-3 A-block">     
           <div class="square untouch" id="A4"></div>        
           
           <div class="square untouch" id="A5"></div>
                     
           <div class="square untouch" id="A6"></div>
        </div>
          
        <div class="col-lg-3 B-block">     
           <div class="square untouch" id="B4"></div>
                    
           <div class="square untouch" id="B5"></div>
                     
           <div class="square untouch" id="B6"></div>
        </div>
           
        <div class="col-lg-3 C-block">     
           <div class="square untouch" id="C4"></div>
                     
           <div class="square untouch" id="C5"></div>
                     
           <div class="square untouch" id="C6"></div>
        </div>
           
      </div>
 
  
      <div class="row">
        <div class="col-lg-3 A-block">  
           <div class="square untouch" id="A7"></div>
           
           <div class="square untouch" id="A8"></div>
          
           <div class="square untouch" id="A9"></div>
        </div>
          
        <div class="col-lg-3 B-block">     
           <div class="square untouch" id="B7"></div>
                     
           <div class="square untouch" id="B8"></div>
                     
           <div class="square untouch" id="B9"></div>
        </div>
           
        <div class="col-lg-3 C-block">     
           <div class="square untouch" id="C7"></div>
                     
           <div class="square untouch" id="C8"></div>
                     
           <div class="square untouch" id="C9"></div>
        </div>
          
      </div>
   <br>
   <div class="row">
         
        <div class="col-lg-3 D-block">          
           <div class="square untouch" id="D1"></div>
                     
           <div class="square untouch" id="D2"></div>
                     
           <div class="square untouch" id="D3"></div>
        </div>
           
        <div class="col-lg-3 E-block">     
           <div class="square untouch" id="E1"></div>
                     
           <div class="square untouch" id="E2"></div>
                    
           <div class="square untouch" id="E3"></div>
        </div>
           
        <div class="col-lg-3 F-block">     
           <div class="square untouch" id="F1"></div>
                     
           <div class="square untouch" id="F2"></div>
                     
           <div class="square untouch" id="F3"></div>
        </div>
           
      </div>
  
  
      <div class="row">
           
        <div class="col-lg-3 D-block">     
           <div class="square untouch" id="D4"></div>        
           
           <div class="square untouch" id="D5"></div>
                     
           <div class="square untouch" id="D6"></div>
        </div>
          
        <div class="col-lg-3 E-block">     
           <div class="square untouch" id="E4"></div>
                     
           <div class="square untouch" id="E5"></div>
                     
           <div class="square untouch" id="E6"></div>
        </div>
           
        <div class="col-lg-3 F-block">     
           <div class="square untouch" id="F4"></div>
                     
           <div class="square untouch" id="F5"></div>
                     
           <div class="square untouch" id="F6"></div>
        </div>
           
      </div>
 
  
      <div class="row">
        <div class="col-lg-3 D-block">  
           <div class="square untouch" id="D7"></div>
           
           <div class="square untouch" id="D8"></div>
          
           <div class="square untouch" id="D9"></div>
        </div>
          
        <div class="col-lg-3 E-block">     
           <div class="square untouch" id="E7"></div>
                     
           <div class="square untouch" id="E8"></div>
                     
           <div class="square untouch" id="E9"></div>
        </div>
           
        <div class="col-lg-3 F-block">     
           <div class="square untouch" id="F7"></div>
                     
           <div class="square untouch" id="F8"></div>
                     
           <div class="square untouch" id="F9"></div>
        </div>
          
      </div>
      <br>
      <div class="row">
         
        <div class="col-lg-3 G-block">          
           <div class="square untouch" id="G1"></div>
                     
           <div class="square untouch" id="G2"></div>
                     
           <div class="square untouch" id="G3"></div>
        </div>
           
        <div class="col-lg-3 H-block">     
           <div class="square untouch" id="H1"></div>
                     
           <div class="square untouch" id="H2"></div>
                     
           <div class="square untouch" id="H3"></div>
        </div>
           
        <div class="col-lg-3 I-block">     
           <div class="square untouch" id="I1"></div>
                     
           <div class="square untouch" id="I2"></div>
                     
           <div class="square untouch" id="I3"></div>
        </div>
           
      </div>
  
  
      <div class="row">
           
        <div class="col-lg-3 G-block">     
           <div class="square untouch" id="G4"></div>        
           
           <div class="square untouch" id="G5"></div>
                     
           <div class="square untouch" id="G6"></div>
        </div>
          
        <div class="col-lg-3 H-block">     
           <div class="square untouch" id="H4"></div>
                     
           <div class="square untouch" id="H5"></div>
                     
           <div class="square untouch" id="H6"></div>
        </div>
           
        <div class="col-lg-3 I-block">     
           <div class="square untouch" id="I4"></div>
                     
           <div class="square untouch" id="I5"></div>
                     
           <div class="square untouch" id="I6"></div>
        </div>
           
      </div>
 
  
      <div class="row">
        <div class="col-lg-3 G-block">  
           <div class="square untouch" id="G7"></div>
           
           <div class="square untouch" id="G8"></div>
          
           <div class="square untouch" id="G9"></div>
        </div>
          
        <div class="col-lg-3 H-block">     
           <div class="square untouch" id="H7"></div>
                     
           <div class="square untouch" id="H8"></div>
                     
           <div class="square untouch" id="H9"></div>
        </div>
           
        <div class="col-lg-3 I-block">     
           <div class="square untouch" id="I7"></div>
                     
           <div class="square untouch" id="I8"></div>
                     
           <div class="square untouch" id="I9"></div>
        </div>
          
      </div>
   
 </div>
 
 <div class="form-actions">
  <br><br><br>
   <h4>Loading...</h4>
   <h1></h1>
   <button type="submit">Main Menu</button>
 </div>

</div>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="Scripts/jquery-ui.js"></script>
    <script src="Scripts/jquery.color.js"></script>
    <script src="Scripts/Gruntfile.js"></script>
    <script src="Scripts/jquery.color.svg-names.js"></script>
    <script src="Scripts/EasyAudioEffects.js"></script>
    <script src="Scripts/PlaySound.js"></script>
    <script src="Scripts/jquery.fireworks.js"></script>
    <script src="Scripts/circletimer.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/highlight.js/8.7/highlight.min.js"></script>
    <script src="Scripts/myscript.js"></script> 
    
        
</body>
</html>
