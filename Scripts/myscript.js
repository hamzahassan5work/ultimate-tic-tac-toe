$(document).ready(function() {
    
    $('#A-block,#B-block,#C-block,#D-block,#E-block,#F-block,#G-block,#H-block,#I-block').hide();
    $('h1,button').hide();
    $('#bar1,#bar2,#avatar1,#avatar2,#timer1,#timer2,input').hide();
    $('.square').addClass('blocked').fadeTo(1,0.1);
    
    $('#timer1').circletimer({onComplete: function(){ TimesUp(); }, onUpdate: function(elapsed){Timer(elapsed);}, clockwise: true, timeout: 12000}); 
    $('#timer2').circletimer({onComplete: function(){ TimesUp(); }, onUpdate: function(elapsed){Timer(elapsed);}, clockwise: true, timeout: 12000});
    
    setTimeout(function(){$('.square').fadeTo(2000,1,function(){$(this).removeClass('blocked');}); // continue..
    $('.jumbotron').css("backgroundImage", "none"); $('h4').text('Player 1 Turns').animate({color: "#dc685a"}); $('#bar1,#bar2').show(1000,function(){$('#avatar1,#avatar2').show(1000,function(){$('input').show(1000,function(){
    $('#timer1').circletimer("start").slideDown(500);});});});},10000);
        
    var p1turn = true;
    var tie = false;
    var X_blocks = [];
    var O_blocks = [];
    
        var A = "#A",
            B = "#B",
            C = "#C",
            D = "#D",
            E = "#E",
            F = "#F",
            G = "#G",
            H = "#H",
            I = "#I";
        
        var CA = ".A-block",
            CB = ".B-block",
            CC = ".C-block",
            CD = ".D-block",
            CE = ".E-block",
            CF = ".F-block",
            CG = ".G-block",
            CH = ".H-block",
            CI = ".I-block";
        
        
    function TimesUp() {
        
        $.stopSound();
        
        $.playSound("Sounds/TimeUp.mp3");
        
        if(p1turn)
            {
                var last_x_myclass = X_blocks.pop();
                $(last_x_myclass).removeClass("X-wins Over").children().removeClass("disable-block blocked").addClass('untouch').text("").css("backgroundColor","#78bec5");
                var last_x_block = X_blocks.pop();
                $(last_x_block).hide(800).text("");
                $('#avatar2,#input2').fadeTo(100,1);
                $('#bar2').fadeTo(100,0.8);
                $("h4").text("Player 2 Turns").animate({color: "#ecaf4f"});  
                $(".untouch").addClass('O-turns').removeClass('disable-block X-turns');
                $('#timer1').hide().circletimer("stop");
                setTimeout(function(){$('#timer2').circletimer("start").slideDown(500);},1000);
                p1turn = false;
            }
        else
            {
                var last_o_myclass = O_blocks.pop();
                $(last_o_myclass).removeClass("O-wins Over").children().removeClass("disable-block blocked").addClass('untouch').text("").css("backgroundColor","#78bec5");
                var last_o_block = O_blocks.pop();
                $(last_o_block).hide(800).text("");
                $('#avatar1,#input1').fadeTo(100,1);
                $('#bar1').fadeTo(100,0.8);
                $("h4").text("Player 1 Turns").animate({color: "#dc685a"}); 
                $(".untouch").addClass('X-turns').removeClass('disable-block O-turns');
                $('#timer2').hide().circletimer("stop");
                setTimeout(function(){$('#timer1').circletimer("start").slideDown(500);},500);
                p1turn = true;
            }
        
    }
    
    function Timer(elapsed){
        
        if(elapsed.toFixed(0) == 7015)
            {
                $.playSound("Sounds/tictic.mp3");
            }
    }
    
    $(".square").hover(
     function () 
     {
         if(p1turn == false){this.innerHTML = "O"; $(this).animate({backgroundColor: "#3d4250", color: "darkgrey"},100);}
         else               {this.innerHTML = "X"; $(this).animate({backgroundColor: "#3d4250", color: "darkgrey"},100);}
              
     },
     function () 
     {
         if($(this).hasClass('blocked') == false) {this.innerHTML = ""; $(this).animate({backgroundColor: "#78bec5"},100);}   
     }
     );     
    
    $(".square").click(function play() {
        
        $.stopSound();
        $.playSound("Sounds/click.mp3");
        
        $(".A-block,.B-block,.C-block,.D-block,.E-block,.F-block,.G-block,.H-block,.I-block").children().addClass('disable-block').removeClass('X-turns O-turns');
        var num = $(this).attr('id');
        var ID = num.substring(1);
        
        if (p1turn)
            {              
                $('#avatar2,#input2').fadeTo(100,1);
                $('#bar2').fadeTo(100,0.8);
                $(this).animate({color: "white", backgroundColor: "#dc685a"});
                this.innerHTML = "X";  
                $("h4").text("Player 2 Turns").animate({color: "#ecaf4f"});                      
                p1turn = false;
                $('#timer1').hide().circletimer("stop");
                $('#timer2').circletimer("start").slideDown(500);
            }
        else
            {            
                $('#avatar1,#input1').fadeTo(100,1);
                $('#bar1').fadeTo(100,0.8);
                $(this).animate({color: "white", backgroundColor: "#ecaf4f"});
                this.innerHTML = "O";
                $("h4").text("Player 1 Turns").animate({color: "#dc685a"});        
                p1turn = true;
                $('#timer2').hide().circletimer("stop");
                $('#timer1').circletimer("start").slideDown(500);
            } 
                
        $(this).addClass('blocked').removeClass('untouch');
        
        result(A, CA);
        result(B, CB);
        result(C, CC);
        result(D, CD);
        result(E, CE);
        result(F, CF);
        result(G, CG);
        result(H, CH);
        result(I, CI);
        
        var WINS = Final_Result();
        
        if(WINS == false)
        {
            if(p1turn){turns(ID, "X-turns");}
            else {turns(ID, "O-turns");}
        }
        else
        {
            $('#timer1').hide().circletimer("stop");
            $('#timer2').hide().circletimer("stop");
            $('h4').text('Game Over!').animate({color: "pink"});
            setTimeout(function(){$('.square,#A-block,#B-block,#C-block,#D-block,#E-block,#F-block,#G-block,#H-block,#I-block').fadeTo(500,0);},1000);
            $('.jumbotron').fadeTo(1700,1,function(){ $(this).css({"backgroundImage": "url(trophy2.gif)","opacity": "0.9"}); $('h1').slideDown(); $('button').slideDown(); if(tie!==true){$('body').fireworks();}});
        }
        
    });
    
    function turns(ID,turn) {
          
        if(ID == "1"){Block_On(".A-block",turn);}
   else if(ID == "2"){Block_On(".B-block",turn);}
   else if(ID == "3"){Block_On(".C-block",turn);}
   else if(ID == "4"){Block_On(".D-block",turn);}
   else if(ID == "5"){Block_On(".E-block",turn);}
   else if(ID == "6"){Block_On(".F-block",turn);}
   else if(ID == "7"){Block_On(".G-block",turn);}
   else if(ID == "8"){Block_On(".H-block",turn);}
   else if(ID == "9"){Block_On(".I-block",turn);}       
             
 }
    
    function Block_On(block,turn) {
        
        if($(block).hasClass('X-wins') || $(block).hasClass('O-wins') || $(block).hasClass('Tie'))
        {
            $(".square").removeClass('disable-block');
            $(".untouch").addClass(turn);
        }
        else
        {
            $(block).children('.untouch').removeClass('disable-block').addClass(turn);
        }
                
    }
    
    function result(id, myclass) {
              
        if($(id+"1").text() == $(id+"2").text() && $(id+"1").text() == $(id+"3").text() && $(id+"2").text() == $(id+"3").text() && $(id+"1").text() != "")
            {
                if($(id+"1").text() == "X"){var wins = "X";  MatchEnd(wins, myclass);}
                else {var wins = "O";  MatchEnd(wins, myclass);}              
            }
   else if($(id+"4").text() == $(id+"5").text() && $(id+"4").text() == $(id+"6").text() && $(id+"5").text() == $(id+"6").text() && $(id+"4").text() != "")
            {
                if($(id+"4").text() == "X"){var wins = "X";  MatchEnd(wins, myclass);}
                else {var wins = "O";  MatchEnd(wins, myclass);}              
            }
   else if($(id+"7").text() == $(id+"8").text() && $(id+"7").text() == $(id+"9").text() && $(id+"8").text() == $(id+"9").text() && $(id+"7").text() != "")
            {
                if($(id+"7").text() == "X"){var wins = "X";  MatchEnd(wins, myclass);}
                else {var wins = "O";  MatchEnd(wins, myclass);}            
            }
   else if($(id+"1").text() == $(id+"4").text() && $(id+"1").text() == $(id+"7").text() && $(id+"4").text() == $(id+"7").text() && $(id+"1").text() != "")
            {
                if($(id+"1").text() == "X"){var wins = "X";  MatchEnd(wins, myclass);}
                else {var wins = "O";  MatchEnd(wins, myclass);}              
            }
   else if($(id+"2").text() == $(id+"5").text() && $(id+"2").text() == $(id+"8").text() && $(id+"5").text() == $(id+"8").text() && $(id+"2").text() != "")
            {
                if($(id+"2").text() == "X"){var wins = "X";  MatchEnd(wins, myclass);}
                else {var wins = "O";  MatchEnd(wins, myclass);}              
            }
   else if($(id+"3").text() == $(id+"6").text() && $(id+"3").text() == $(id+"9").text() && $(id+"6").text() == $(id+"9").text() && $(id+"3").text() != "")
            {
                if($(id+"3").text() == "X"){var wins = "X";  MatchEnd(wins, myclass);}
                else {var wins = "O";  MatchEnd(wins, myclass);}              
            }
   else if($(id+"1").text() == $(id+"5").text() && $(id+"1").text() == $(id+"9").text() && $(id+"5").text() == $(id+"9").text() && $(id+"1").text() != "")
            {
                if($(id+"1").text() == "X"){var wins = "X";  MatchEnd(wins, myclass);}
                else {var wins = "O";  MatchEnd(wins, myclass);}               
            }
   else if($(id+"3").text() == $(id+"5").text() && $(id+"3").text() == $(id+"7").text() && $(id+"5").text() == $(id+"7").text() && $(id+"3").text() != "")
            {
                if($(id+"3").text() == "X"){var wins = "X";  MatchEnd(wins, myclass);}
                else {var wins = "O";  MatchEnd(wins, myclass);}               
            }
    else if($(id+"1").text() != "" && $(id+"2").text() != "" && $(id+"3").text() != "" && $(id+"4").text() != "" && $(id+"5").text() != "" && $(id+"6").text() != "" && $(id+"7").text() != "" && $(id+"8").text() != "" && $(id+"9").text() != "")
        {
                var wins = "Tie";  MatchEnd(wins, myclass);        
        }
        
    }
    
    function MatchEnd(wins, myclass) {
        
        var myID = "#" + myclass.substring(1);
        
        if(wins == "X")
            {
                $(myclass).addClass("X-wins Over").children().addClass("disable-block blocked").removeClass('untouch');
                $(myID).animate({backgroundColor: "#dc685a"}).show(800,function() {$(myID).text('X');});
                var X_push = true;
                X_blocks.filter(function(idclass){
                    if(idclass == myID) 
                    {
                        X_push = false;
                    }           
                });
                
                if(X_push)
                {
                    X_blocks.push(myID);
                    X_blocks.push(myclass);
                    $.playSound("Sounds/BWin2.mp3");
                }
                            
            }
       else if(wins == "O")
            {
                $(myclass).addClass("O-wins Over").children().addClass("disable-block blocked").removeClass('untouch');
                $(myID).animate({backgroundColor: "#ecaf4f"}).show(800,function() {$(myID).text('O');});
                var O_push = true;
                O_blocks.filter(function(idclass){
                    if(idclass == myID) 
                    {
                        O_push = false;
                    }               
                });
                
                if(O_push)
                {
                    O_blocks.push(myID);
                    O_blocks.push(myclass);
                    $.playSound("Sounds/BWin.mp3");
                }
                
            }
        else
            {
                $(myclass).addClass("Tie Over").children().addClass("disable-block blocked").fadeTo(1000,0.4).removeClass('untouch');
            }
        
    }
    
    function Final_Result() {
        
            if($('.A-block').hasClass('X-wins') && $('.B-block').hasClass('X-wins') && $('.C-block').hasClass('X-wins') || $('.A-block').hasClass('O-wins') && $('.B-block').hasClass('O-wins') && $('.C-block').hasClass('O-wins'))
            {
                    if($('.A-block').hasClass('X-wins')){$('h1').text("Player 1 Wins!"); Add_Score("p1");}
               else if($('.A-block').hasClass('O-wins')){$('h1').text("Player 2 Wins!"); Add_Score("p2");}
                $('.square').addClass('blocked').css("border","1px thin cadetblue"); return true;
            }
       else if($('.D-block').hasClass('X-wins') && $('.E-block').hasClass('X-wins') && $('.F-block').hasClass('X-wins') || $('.D-block').hasClass('O-wins') && $('.E-block').hasClass('O-wins') && $('.F-block').hasClass('O-wins'))
            {
                    if($('.D-block').hasClass('X-wins')){$('h1').text("Player 1 Wins!"); Add_Score("p1");}
               else if($('.D-block').hasClass('O-wins')){$('h1').text("Player 2 Wins!"); Add_Score("p2");}
                $('.square').addClass('blocked').css("border","1px thin cadetblue"); return true;
            }
       else if($('.G-block').hasClass('X-wins') && $('.H-block').hasClass('X-wins') && $('.I-block').hasClass('X-wins') || $('.G-block').hasClass('O-wins') && $('.H-block').hasClass('O-wins') && $('.I-block').hasClass('O-wins'))
            {
                    if($('.G-block').hasClass('X-wins')){$('h1').text("Player 1 Wins!"); Add_Score("p1");}
               else if($('.G-block').hasClass('O-wins')){$('h1').text("Player 2 Wins!"); Add_Score("p2");}
                $('.square').addClass('blocked').css("border","1px thin cadetblue"); return true;
            }
       else if($('.A-block').hasClass('X-wins') && $('.D-block').hasClass('X-wins') && $('.G-block').hasClass('X-wins') || $('.A-block').hasClass('O-wins') && $('.D-block').hasClass('O-wins') && $('.G-block').hasClass('O-wins'))
            {
                    if($('.A-block').hasClass('X-wins')){$('h1').text("Player 1 Wins!"); Add_Score("p1");}
               else if($('.A-block').hasClass('O-wins')){$('h1').text("Player 2 Wins!"); Add_Score("p2");}
                $('.square').addClass('blocked').css("border","1px thin cadetblue"); return true;
            }
        if($('.B-block').hasClass('X-wins') && $('.E-block').hasClass('X-wins') && $('.H-block').hasClass('X-wins') || $('.B-block').hasClass('O-wins') && $('.E-block').hasClass('O-wins') && $('.H-block').hasClass('O-wins'))
            {
                    if($('.B-block').hasClass('X-wins')){$('h1').text("Player 1 Wins!"); Add_Score("p1");}
               else if($('.B-block').hasClass('O-wins')){$('h1').text("Player 2 Wins!"); Add_Score("p2");}
                $('.square').addClass('blocked').css("border","1px thin cadetblue"); return true;
            }
       else if($('.C-block').hasClass('X-wins') && $('.F-block').hasClass('X-wins') && $('.I-block').hasClass('X-wins') || $('.C-block').hasClass('O-wins') && $('.F-block').hasClass('O-wins') && $('.I-block').hasClass('O-wins'))
            {
                    if($('.C-block').hasClass('X-wins')){$('h1').text("Player 1 Wins!"); Add_Score("p1");}
               else if($('.C-block').hasClass('O-wins')){$('h1').text("Player 2 Wins!"); Add_Score("p2");}
                $('.square').addClass('blocked').css("border","1px thin cadetblue"); return true;
            }
       else if($('.A-block').hasClass('X-wins') && $('.E-block').hasClass('X-wins') && $('.I-block').hasClass('X-wins') || $('.A-block').hasClass('O-wins') && $('.E-block').hasClass('O-wins') && $('.I-block').hasClass('O-wins'))
            {
                    if($('.A-block').hasClass('X-wins')){$('h1').text("Player 1 Wins!"); Add_Score("p1");}
               else if($('.A-block').hasClass('O-wins')){$('h1').text("Player 2 Wins!"); Add_Score("p2");}
                $('.square').addClass('blocked').css("border","1px thin cadetblue"); return true;
            }
       else if($('.C-block').hasClass('X-wins') && $('.E-block').hasClass('X-wins') && $('.G-block').hasClass('X-wins') || $('.C-block').hasClass('O-wins') && $('.E-block').hasClass('O-wins') && $('.G-block').hasClass('O-wins'))
            {
                    if($('.C-block').hasClass('X-wins')){$('h1').text("Player 1 Wins!"); Add_Score("p1");}
               else if($('.C-block').hasClass('O-wins')){$('h1').text("Player 2 Wins!"); Add_Score("p2");}
                $('.square').addClass('blocked').css("border","1px thin cadetblue"); return true;
            }
       else if($('.A-block').hasClass('Over') && $('.B-block').hasClass('Over') && $('.C-block').hasClass('Over') && $('.D-block').hasClass('Over') && $('.E-block').hasClass('Over') && $('.F-block').hasClass('Over') && $('.G-block').hasClass('Over') && $('.H-block').hasClass('Over') && $('.I-block').hasClass('Over'))
            {
                tie = true;
                $('h1').text("Match Ties!"); 
                $('.square').addClass('blocked').css("border","1px thin cadetblue");
                Add_Score("tie");
                return true;
            }
        else
            {
                return false;
            }
                  
    }
    
    $("button").click(function reload() {
        
            window.location = "index.html";       
    });
    
    function Add_Score(win)
    {
          var p1 = $("#input1").val();
          var p2 = $("#input2").val();
          var xhttp = new XMLHttpRequest();
          xhttp.onreadystatechange = function() 
          {
            if (this.readyState == 4 && this.status == 200) 
            {
              //alert(this.responseText);
            }
          };
          xhttp.open("GET", "addscore.php?p1="+p1+"&p2="+p2+"&win="+win, true);
          xhttp.send(); 
    }
    
    
});