<?php

$conn = new mysqli('localhost', 'root', '', 'ultimate ttt');
    
if ($conn->connect_error) 
{
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * FROM stats ORDER BY WR DESC";

$rslt = $conn->query($sql);

if ($rslt->num_rows > 0) 
{
    echo '
                <table class="table table-hover" style="width:80%; margin:auto; font-size:15px;">
                <thead>
                      <tr>
                        <th>Players Name</th>
                        <th>Played</th>
                        <th>Won</th>
                        <th>Drawn</th>
                        <th>Lost</th>
                        <th>WR</th>
                      </tr></thead><tbody>';
        
        while($row = $rslt->fetch_assoc()) 
        {
            echo ' 
                      <tr class="active">
                        <td>'.$row["username"].'</td>
                        <td>'.$row["played"].'</td>
                        <td>'.$row["win"].'</td>
                        <td>'.$row["draw"].'</td>
                        <td>'.$row["lost"].'</td>
                        <td style="background-color:lightgrey">'.$row["WR"].' %'.'</td>
                      </tr>';
        }
    echo '</tbody></table>';
    
}


?>